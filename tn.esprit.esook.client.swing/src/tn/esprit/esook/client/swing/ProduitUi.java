package tn.esprit.esook.client.swing;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.RowFilter;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import tn.edu.esprit.Pidev.Entity.Poduit;
import tn.edu.esprit.Pidev.services.GestionProduitRemote;

public class ProduitUi extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private DefaultTableModel model;
	private JTable tableAffichage;
	TableRowSorter<TableModel> sorter;
	private JButton btnSupprimer;
	private JButton btnChercher;
	private JTextField textFieldChercher;
	private JTextField textfieldUpdateNom;
	private JTextField textFieldUpdateDesc;
	private JTextField textFieldUpdateQt;
	private JTextField textFieldUpDatePrice;
	private JButton btnUpdate;
	private JButton btnValidate;
	private JButton btnBack;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ProduitUi frame = new ProduitUi();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public void loadTableData() { // //affichage de la liste des produits

		try {
			model = new DefaultTableModel(

			new Object[][] {

			}, new String[] {

			"Nom Produit", "Description", "Quantit�", "Prix" });
			System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
			Context ctx = new InitialContext();
			GestionProduitRemote gestProd = (GestionProduitRemote) ctx
					.lookup("/Pidev/GestionProduit!tn.edu.esprit.Pidev.services.GestionProduitRemote");
			List<Poduit> allProduit = gestProd.allProduit();

			for (Poduit p : allProduit) {

				model.addRow(new Object[] { p.getIdProduit(), p.getNomProd(),
						p.getDescription(), p.getQtStoke(), p.getPrice() });
			}
			tableAffichage.setModel(model);
			sorter = new TableRowSorter<TableModel>(model);
			// sorter.setRowFilter(filter);
			tableAffichage.setRowSorter(sorter);

		} catch (NamingException e3) {
			// TODO Auto-generated catch block
			e3.printStackTrace();
		}

	}

	// fin de l'affichage

	/**
	 * Create the frame.
	 */
	public ProduitUi() {
		this.pack();
		this.setLocationRelativeTo(null);
		this.setResizable(false);

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 813, 463);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setForeground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);

		tableAffichage = new JTable();

		tableAffichage.setBackground(Color.LIGHT_GRAY);
		tableAffichage.setBounds(78, 82, 643, 250);
		tableAffichage.setModel(new DefaultTableModel(new Object[][] {
				{ null, null, null, null }, { null, null, null, null },
				{ null, null, null, null }, }, new String[] { "Nom Produit",
				"Description", "Quantit\u00E9", "   Prix" }));
		tableAffichage.getColumnModel().getColumn(0).setPreferredWidth(92);
		tableAffichage.getColumnModel().getColumn(0).setMinWidth(92);
		tableAffichage.getColumnModel().getColumn(1).setPreferredWidth(150);
		tableAffichage.getColumnModel().getColumn(2).setPreferredWidth(55);
		tableAffichage.getColumnModel().getColumn(3).setPreferredWidth(56);

		JButton ButtonAffiche = new JButton("View");
		ButtonAffiche.setBounds(437, 22, 71, 23);
		ButtonAffiche.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Poduit p = new Poduit();
				Context ctx;
				try {
					ctx = new InitialContext();
					GestionProduitRemote gestProd = (GestionProduitRemote) ctx
							.lookup("/Pidev/GestionProduit!tn.edu.esprit.Pidev.services.GestionProduitRemote");
					loadTableData();
				} catch (NamingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		contentPane.setLayout(null);
		contentPane.add(ButtonAffiche);

		btnSupprimer = new JButton("Delete");
		btnSupprimer.setBounds(78, 362, 83, 23);
		btnSupprimer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int selectedRow = tableAffichage.getSelectedRow();
				int id = Integer.parseInt(tableAffichage.getValueAt(
						selectedRow, 0).toString());
				Context ctx;

				try {
					ctx = new InitialContext();
					GestionProduitRemote gestProd = (GestionProduitRemote) ctx
							.lookup("/Pidev/GestionProduit!tn.edu.esprit.Pidev.services.GestionProduitRemote");
					Poduit p = gestProd.SearchProduit(id);
					System.out.println(p.getIdProduit());
					gestProd.RemoveProduit(p);

					loadTableData();

				} catch (NamingException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

			}
		});
		contentPane.add(btnSupprimer);
		contentPane.add(tableAffichage);

		btnChercher = new JButton("Search");
		btnChercher.setBounds(311, 22, 83, 23);
		btnChercher.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Start Filter");
				String text = textFieldChercher.getText();

				RowFilter<TableModel, Object> rf = null;
				// If current expression doesn't parse, don't update.
				try {
					rf = RowFilter.regexFilter(text);
				} catch (java.util.regex.PatternSyntaxException e1) {
					return;
				}
				sorter.setRowFilter(rf);

			}
		});

		/*
		 * btnUpdate = new JButton("Update"); btnUpdate.addActionListener(new
		 * ActionListener() { public void actionPerformed(ActionEvent arg0) {
		 * try{ btnUpdate.setVisible(true); int ligneSelectionne =
		 * tableAffichage.getSelectedRow(); int id
		 * =Integer.parseInt(tableAffichage
		 * .getValueAt(ligneSelectionne,0).toString()); Context ctx;
		 * 
		 * 
		 * ctx = new InitialContext(); GestionProduitRemote gestProd =
		 * (GestionProduitRemote) ctx .lookup(
		 * "/Pidev/GestionProduit!tn.edu.esprit.Pidev.services.GestionProduitRemote"
		 * ); Poduit p=gestProd.SearchProduit(id);
		 * System.out.println(p.getIdProduit()); gestProd.SearchProduit(id);
		 * textfieldUpdateNom.setText(p.getNomProd());
		 * textFieldUpdateDesc.setText(p.getDescription());
		 * textFieldUpdateQt.setText(p.getQtStoke().toString());
		 * textFieldUpDatePrice.setText(p.getPrice().toString());
		 * textfieldUpdateNom.setVisible(true);
		 * textFieldUpdateDesc.setVisible(true);
		 * textFieldUpdateQt.setVisible(true);
		 * textFieldUpDatePrice.setVisible(true); gestProd.UpdateProduit(p);
		 * loadTableData(); }catch (Exception e2){
		 * 
		 * 
		 * JOptionPane.showMessageDialog(null,
		 * "selectionnez le produit a modifier"
		 * ,"error",JOptionPane.ERROR_MESSAGE); }}});
		 * 
		 * contentPane.add(btnUpdate);
		 * 
		 * 
		 * loadTableData(); }
		 */

		textFieldChercher = new JTextField();
		textFieldChercher.setBounds(78, 23, 171, 20);

		contentPane.add(textFieldChercher);
		textFieldChercher.setColumns(10);
		contentPane.add(btnChercher);

		textfieldUpdateNom = new JTextField();
		textfieldUpdateNom.setBounds(180, 363, 86, 20);
		contentPane.add(textfieldUpdateNom);
		textfieldUpdateNom.setColumns(10);

		textFieldUpdateDesc = new JTextField();
		textFieldUpdateDesc.setBounds(291, 363, 86, 20);
		contentPane.add(textFieldUpdateDesc);
		textFieldUpdateDesc.setColumns(10);

		textFieldUpdateQt = new JTextField();
		textFieldUpdateQt.setBounds(399, 363, 86, 20);
		contentPane.add(textFieldUpdateQt);
		textFieldUpdateQt.setColumns(10);

		textFieldUpDatePrice = new JTextField();
		textFieldUpDatePrice.setBounds(515, 363, 86, 20);
		contentPane.add(textFieldUpDatePrice);
		textFieldUpDatePrice.setColumns(10);

		btnUpdate = new JButton("update");
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					btnUpdate.setVisible(true);
					int ligneSelectionne = tableAffichage.getSelectedRow();
					int id = Integer.parseInt(tableAffichage.getValueAt(
							ligneSelectionne, 0).toString());
					System.out.println(id);
					Context ctx;

					ctx = new InitialContext();
					GestionProduitRemote gestProd = (GestionProduitRemote) ctx
							.lookup("/Pidev/GestionProduit!tn.edu.esprit.Pidev.services.GestionProduitRemote");
					Poduit p = gestProd.SearchProduit(id);
					System.out.println(p.getIdProduit());
					// gestProd.SearchProduit(id);
					// Poduit p = new Poduit();
					// p.setNomProd(textfieldUpdateNom.getText());
					// p.setDescription(textFieldUpdateDesc.getText());
					// gestProd.UpdateProduit(p);

					// System.out.println(p.getIdProduit());
					// System.out.println(p.getNomProd());

					// gestProd.SearchProduit(id);

					textfieldUpdateNom.setText(p.getNomProd());
					textfieldUpdateNom.setVisible(true);

					// System.out.println(p.getNomProd());
					textFieldUpdateDesc.setText(p.getDescription());
					textFieldUpdateQt.setText(p.getQtStoke().toString());
					textFieldUpDatePrice.setText(p.getPrice().toString());

					// System.out.println(p.getNomProd());

					textFieldUpdateDesc.setVisible(true);
					textFieldUpdateQt.setVisible(true);
					textFieldUpDatePrice.setVisible(true);

				} catch (Exception e2) {

					JOptionPane.showMessageDialog(null,
							"selectionnez le produit a modifier", "error",
							JOptionPane.ERROR_MESSAGE);
				}

			}
		});

		contentPane.add(btnUpdate);
		loadTableData();

		btnUpdate.setBounds(632, 362, 89, 23);
		contentPane.add(btnUpdate);

		btnValidate = new JButton("validate");
		btnValidate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					DefaultTableModel model = (DefaultTableModel) tableAffichage
							.getModel();
					int ligneSelectionne = tableAffichage.getSelectedRow();
					int id = (Integer) tableAffichage.getValueAt(
							ligneSelectionne, 0);

					Context ctx;

					ctx = new InitialContext();
					GestionProduitRemote gestProd = (GestionProduitRemote) ctx
							.lookup("/Pidev/GestionProduit!tn.edu.esprit.Pidev.services.GestionProduitRemote");
					Poduit p = gestProd.SearchProduit(id);
					p.setNomProd(textfieldUpdateNom.getText());
					p.setDescription(textFieldUpdateDesc.getText());
					gestProd.UpdateProduit(p);

					ProduitUi produitui = new ProduitUi();
					produitui.setVisible(true);
					setVisible(false);

				} catch (Exception e2) {

					JOptionPane.showMessageDialog(null,
							"selectionnez le produit a modifier", "error",
							JOptionPane.ERROR_MESSAGE);
				}

			}
		});
		btnValidate.setBounds(632, 390, 89, 23);
		contentPane.add(btnValidate);

		btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
				UserInterface userInterface = new UserInterface();
				userInterface.setVisible(true);
			}
		});
		btnBack.setBounds(708, 0, 89, 23);
		contentPane.add(btnBack);
	}

}
