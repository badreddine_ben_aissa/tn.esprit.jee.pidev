package tn.esprit.esook.client.swing;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.RowFilter;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import tn.edu.esprit.Pidev.Entity.Account;
import tn.edu.esprit.Pidev.Entity.User;
import tn.esprit.esook.client.swing.delegater.UserDelegate;

public class usergui2 extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private DefaultTableModel model;
	private JTable tableUseryui;
	TableRowSorter<TableModel> sorter;
	private JTextField textField;
	/* User user; */
	Account account;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					usergui2 frame = new usergui2(null);
					frame.setVisible(true);
					frame.setLocationRelativeTo(null);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public void loadTableData() {
		model = new DefaultTableModel(new Object[][] {

		}, new String[] { "id user", "Last name", "First name", "adresse" });

		List<User> listUser = UserDelegate.retrieveAllUsers();

		for (User u : listUser) {

			model.addRow(new Object[] { u.getIdAccount(), u.getFirstName(),
					u.getLastName(), u.getAdress() });
		}
		tableUseryui.setModel(model);
		sorter = new TableRowSorter<TableModel>(model);
		tableUseryui.setRowSorter(sorter);

	}

	/**
	 * Create the frame.
	 */
	public usergui2(Account a) {
		setTitle("E-commerce");
		this.account = a;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 672, 466);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 125, 612, 274);
		contentPane.add(scrollPane);
		tableUseryui = new JTable();
		scrollPane.setViewportView(tableUseryui);

		final JComboBox comboBox = new JComboBox();
		comboBox.setBounds(120, 67, 112, 31);
		comboBox.setForeground(Color.BLACK);
		comboBox.setModel(new DefaultComboBoxModel(new String[] { "Id User",
				"Last name" }));
		contentPane.add(comboBox);

		textField = new JTextField();
		textField.setBounds(10, 67, 100, 31);
		contentPane.add(textField);
		textField.setColumns(10);

		JButton btnFind = new JButton("Find");
		btnFind.setBounds(242, 66, 112, 33);
		btnFind.setForeground(Color.BLACK);

		btnFind.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				System.out.println("Start Filter");
				String text = textField.getText();
				System.out.println(text);

				int type = comboBox.getSelectedIndex();
				RowFilter<TableModel, Object> rf = null;
				try {
					rf = RowFilter.regexFilter(text, type);

				} catch (java.util.regex.PatternSyntaxException e1) {

					return;
				}
				sorter.setRowFilter(rf);

			}
		});
		contentPane.add(btnFind);

		JButton btnNewButton = new JButton("Back");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
				UserInterface userinterface = new UserInterface();
				userinterface.setVisible(true);
			}
		});
		btnNewButton.setBounds(364, 66, 89, 33);
		contentPane.add(btnNewButton);

		loadTableData();
	}
}
