package tn.esprit.esook.client.swing;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import tn.edu.esprit.Pidev.Entity.Account;
import tn.edu.esprit.Pidev.Entity.Admin;
import tn.edu.esprit.Pidev.Entity.User;
import tn.esprit.esook.client.swing.delegater.AccountDelegate;

public class Authentification extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static Account account;
	public static User user;
	public static String role;

	private JPanel contentPane;
	private JTextField login;
	private JPasswordField passwordField;
	public static Admin admin;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Authentification frame = new Authentification();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Authentification() {
		this.pack();
		this.setLocationRelativeTo(null);
		this.setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 502, 324);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		JPanel panel = new JPanel();
		panel.setForeground(Color.WHITE);
		panel.setBackground(Color.WHITE);
		contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(null);

		JButton btnNewButton_1 = new JButton("inscription");
		btnNewButton_1.setBackground(Color.LIGHT_GRAY);
		btnNewButton_1.setToolTipText("");
		btnNewButton_1.setFont(new Font("SketchFlow Print", Font.PLAIN, 12));
		btnNewButton_1.setForeground(new Color(139, 0, 0));
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AddAdmin addAdmin = new AddAdmin();
				addAdmin.setVisible(true);
			}
		});
		btnNewButton_1.setBounds(111, 170, 105, 23);
		panel.add(btnNewButton_1);

		JButton btnNewButton = new JButton("Login");
		btnNewButton.setBackground(Color.LIGHT_GRAY);
		btnNewButton.setForeground(new Color(139, 0, 0));
		btnNewButton.setFont(new Font("SketchFlow Print", Font.PLAIN, 12));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {

					String loginRec = login.getText();
					String passwordRec = passwordField.getText();

					if (AccountDelegate.authentication(loginRec, passwordRec) instanceof Admin) {

						account = AccountDelegate.authentication(loginRec,
								passwordRec);

						role = "admin";
						JFrame message = new JFrame();
						JOptionPane.showMessageDialog(message,
								"Hello !!!" + account.getFirstName() + " "
										+ account.getLastName());
						setVisible(false);
						UserInterface interface1 = new UserInterface();
						interface1.setVisible(true);
					} else if (AccountDelegate.authentication(loginRec,
							passwordRec) instanceof User) {
						account = AccountDelegate.authentication(loginRec,
								passwordRec);
						user = (User) AccountDelegate.authentication(loginRec,
								passwordRec);
						role = "user";
						JFrame message = new JFrame();
						JOptionPane.showMessageDialog(message,
								"Hello !!!" + account.getFirstName() + " "
										+ account.getLastName());
						setVisible(false);
						UserInterface interface1 = new UserInterface();
						interface1.setVisible(true);
					} else if (login.getText().equals("")
							|| passwordField.getText().equals("")) {

						JFrame message = new JFrame();

						JOptionPane.showMessageDialog(message,
								"Please enter Login and Password");
					} else {
						JFrame message = new JFrame();
						JOptionPane.showMessageDialog(message, "please SignUP");

					}

				} catch (Exception e2) {
					e2.printStackTrace();

				}

			}
		});
		btnNewButton.setBounds(240, 170, 98, 23);
		panel.add(btnNewButton);

		JLabel lblLogin = new JLabel("Login");
		lblLogin.setForeground(Color.WHITE);
		lblLogin.setFont(new Font("SketchFlow Print", Font.BOLD, 13));
		lblLogin.setBounds(22, 36, 54, 14);
		panel.add(lblLogin);

		JLabel lblPassword = new JLabel("Password");
		lblPassword.setForeground(Color.WHITE);
		lblPassword.setFont(new Font("SketchFlow Print", Font.BOLD, 13));
		lblPassword.setBounds(22, 70, 88, 14);
		panel.add(lblPassword);

		login = new JTextField();
		login.setForeground(Color.BLACK);
		login.setBounds(152, 67, 142, 20);
		panel.add(login);
		login.setColumns(10);

		passwordField = new JPasswordField();
		passwordField.setForeground(Color.BLACK);
		passwordField.setBackground(Color.WHITE);
		passwordField.setBounds(152, 120, 142, 20);
		panel.add(passwordField);

		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setBounds(0, 0, 464, 308);
		panel.add(lblNewLabel);
		lblNewLabel.setIcon(new ImageIcon("G:\\BI\\img\\authentif.jpg"));

		JPanel panel_1 = new JPanel();
		panel_1.setBounds(219, 255, 10, 10);
		panel.add(panel_1);
	}
}
