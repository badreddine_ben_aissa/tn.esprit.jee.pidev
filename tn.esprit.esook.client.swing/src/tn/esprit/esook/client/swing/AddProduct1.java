package tn.esprit.esook.client.swing;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import tn.edu.esprit.Pidev.Entity.Account;
import tn.edu.esprit.Pidev.Entity.Category;
import tn.edu.esprit.Pidev.Entity.Poduit;
import tn.edu.esprit.Pidev.Entity.User;
import tn.edu.esprit.Pidev.services.GestionCategoryRemote;
import tn.edu.esprit.Pidev.services.GestionProduitRemote;
import tn.edu.esprit.Pidev.services.GestionUserRemote;
import tn.esprit.esook.client.swing.util.Utils;
import javax.swing.border.BevelBorder;

public class AddProduct1 extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField textFieldNom;
	private JTextField textFieldPrice;
	private JTextField textFieldquantite;
	Account account;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AddProduct1 frame = new AddProduct1(null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */

	public AddProduct1(final Account account) {
		this.pack();
		this.setLocationRelativeTo(null);
		this.setResizable(false);
		this.account = account;

		Utils.initilize(this);

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 529, 468);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblNewLabel_1 = new JLabel("Name :");
		lblNewLabel_1.setForeground(new Color(0, 0, 0));
		lblNewLabel_1.setBounds(20, 34, 79, 25);
		contentPane.add(lblNewLabel_1);

		JLabel lblNewLabel_2 = new JLabel("quantity :");
		lblNewLabel_2.setBounds(20, 234, 56, 14);
		contentPane.add(lblNewLabel_2);

		JLabel lblNewLabel_3 = new JLabel("Description :");
		lblNewLabel_3.setBounds(20, 104, 79, 26);
		contentPane.add(lblNewLabel_3);

		JLabel lblNewLabel_4 = new JLabel("Price :");
		lblNewLabel_4.setBounds(20, 175, 56, 14);
		contentPane.add(lblNewLabel_4);

		final JComboBox comboCategory = new JComboBox();
		comboCategory.setModel(new DefaultComboBoxModel(
				new String[] { "Categories" }));
		comboCategory.setBounds(245, 293, 90, 20);
		contentPane.add(comboCategory);

		textFieldNom = new JTextField();
		textFieldNom.setBounds(145, 36, 190, 20);
		contentPane.add(textFieldNom);
		textFieldNom.setColumns(10);

		final JTextArea textArea = new JTextArea();
		textArea.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		textArea.setWrapStyleWord(true);
		textArea.setForeground(Color.BLACK);
		textArea.setBounds(147, 92, 188, 50);
		contentPane.add(textArea);

		textFieldPrice = new JTextField();
		textFieldPrice.setBounds(145, 172, 190, 20);
		contentPane.add(textFieldPrice);
		textFieldPrice.setColumns(10);

		textFieldquantite = new JTextField();
		textFieldquantite.setBounds(145, 231, 190, 20);
		contentPane.add(textFieldquantite);
		textFieldquantite.setColumns(10);

		JButton btnAdd = new JButton("ADD");
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Context ctx;
				try {
					ctx = new InitialContext();
					GestionProduitRemote gestProd = (GestionProduitRemote) ctx
							.lookup("/Pidev/GestionProduit!tn.edu.esprit.Pidev.services.GestionProduitRemote");
					GestionUserRemote gestUser = (GestionUserRemote) ctx
							.lookup("/Pidev/GestionUser!tn.edu.esprit.Pidev.services.GestionUserRemote");
					GestionCategoryRemote gestcateg = (GestionCategoryRemote) ctx
							.lookup("/Pidev/GestionCategory!tn.edu.esprit.Pidev.services.GestionCategoryRemote");
					User user1 = (User) account;
					Category cat1;// = gestcateg.searchcategoryByid(1);
					int id = comboCategory.getSelectedIndex();
					String name = comboCategory.getItemAt(id).toString();
					List<Category> ls =gestcateg.searchisByName(name);
					cat1 = ls.get(0);

					
					Poduit prod1 = new Poduit();
					prod1.setNomProd(textFieldNom.getText());
					prod1.setPrice(Double.parseDouble(textFieldPrice.getText()));
					prod1.setDescription(textArea.getText());
					prod1.setQtStoke(Integer.parseInt(textFieldquantite
							.getText()));
					prod1.setCategory(cat1);
					prod1.setUser(user1);

					System.out.println(prod1.getCategory().getNameCategory());
					System.out.println(cat1.getIdCategory());
					gestProd.persistProduit(prod1);
				} catch (NamingException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

			
			dispose();
			UserInterface interface1=new UserInterface();
			interface1.setVisible(true);
			}
		});
		Context ctx;
		try {

			ctx = new InitialContext();

			GestionCategoryRemote gestcateg = (GestionCategoryRemote) ctx
					.lookup("/Pidev/GestionCategory!tn.edu.esprit.Pidev.services.GestionCategoryRemote");

			List<Category> listcategory = gestcateg.allcategory();
			for (int i = 0; i < listcategory.size(); i++) {
				comboCategory.addItem(listcategory.get(i).getNameCategory());

			}

		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		btnAdd.setBounds(246, 365, 89, 23);
		contentPane.add(btnAdd);

		JButton btnNewButton = new JButton("cancel");
		btnNewButton.setBounds(132, 365, 89, 23);
		contentPane.add(btnNewButton);
		btnNewButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				dispose();
				UserInterface interface1=new UserInterface();
				interface1.setVisible(true);
				
			}
		});
	}
}
