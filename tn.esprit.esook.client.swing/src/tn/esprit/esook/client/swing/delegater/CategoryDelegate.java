package tn.esprit.esook.client.swing.delegater;

import java.util.List;

import tn.edu.esprit.Pidev.Entity.Category;
import tn.edu.esprit.Pidev.services.GestionCategoryRemote;
import tn.esprit.esook.client.swing.util.ServiceLocator;

/**
 * 
 * @author Amine
 * 
 */
public class CategoryDelegate {
	//
	private static GestionCategoryRemote proxy = (GestionCategoryRemote) ServiceLocator
			.getInstance()
			.getProxy(
					"/Pidev/GestionCategory!tn.edu.esprit.Pidev.services.GestionCategoryRemote");

	public static List<Category> retrieveAllCategories() {
		return proxy.allcategory();
	}
		
	public static void createCategory(Category category){
		proxy.persistCategory(category);
	}
	public static void deleteCategory(Category category){
		proxy.removeCategory(category);
	}
	public static void updateCategory(Category category){
		proxy.updateCategory(category);
	}
	public static Category retrieveOneCategory(int id){
		
		return proxy.searchcategoryByid(id);
	}
public static Category retrieveOneCategoryByName(String name){
		
		return proxy.searchcategoryByName(name);
	}
	
}
