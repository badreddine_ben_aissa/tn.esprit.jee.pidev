package tn.esprit.esook.client.swing.delegater;

import tn.edu.esprit.Pidev.Entity.Admin;
import tn.edu.esprit.Pidev.Entity.User;
import tn.edu.esprit.Pidev.services.GestionAdminRemote;
import tn.esprit.esook.client.swing.util.ServiceLocator;

/**
 * 
 * @author Amine
 * 
 */
public class AdminDelegate {
	//
	private static GestionAdminRemote proxy = (GestionAdminRemote) ServiceLocator
			.getInstance()
			.getProxy(
					"Pidev/GestionAdmin!tn.edu.esprit.Pidev.services.GestionAdminRemote");

//	public static List<User> retrieveAllUsers() {
//		return proxy.getAllUsers();
//	}

	public static void createUser(Admin admin) {
		proxy.persistAdmin(admin);
	}

	public static void deleteAdmin(Admin admin) {
		proxy.removeAdmin(admin);
	}

	public static void updateAdmin(Admin admin) {
		proxy.updateAdmin(admin);
	}

	public static Admin retrieveOneAdmin(int id) {

		return proxy.searchadminByid(id);
	}
	

}
