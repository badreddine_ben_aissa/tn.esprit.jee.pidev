package tn.esprit.esook.client.swing.delegater;

import java.util.List;

import tn.edu.esprit.Pidev.Entity.Poduit;
import tn.edu.esprit.Pidev.services.GestionProduitRemote;
import tn.esprit.esook.client.swing.util.ServiceLocator;

/**
 * 
 * @author Amine
 * 
 */
public class ProductDelegate {
	//
	private static GestionProduitRemote proxy = (GestionProduitRemote) ServiceLocator
			.getInstance()
			.getProxy(
					"Pidev/GestionProduit!tn.edu.esprit.Pidev.services.GestionProduitRemote");

	public static List<Poduit> retrieveAllProduct() {
		return proxy.allProduit();
	}

	public static void createProduct(Poduit poduit) {
		proxy.persistProduit(poduit);
	}

	public static void deleteProduct(Poduit poduit) {
		proxy.RemoveProduit(poduit);
	}

	public static void updateProduct(Poduit poduit) {
		proxy.UpdateProduit(poduit);
	}

	public static Poduit retrieveOneProduct(int idProduit) {

		return proxy.SearchProduit(idProduit);
	}

}
