package tn.esprit.esook.client.swing.delegater;

import java.util.List;

import tn.edu.esprit.Pidev.Entity.User;
import tn.edu.esprit.Pidev.services.GestionUserRemote;
import tn.esprit.esook.client.swing.util.ServiceLocator;

/**
 * 
 * @author Amine
 * 
 */
public class UserDelegate {
	//
	private static GestionUserRemote proxy = (GestionUserRemote) ServiceLocator
			.getInstance()
			.getProxy(
					"/Pidev/GestionUser!tn.edu.esprit.Pidev.services.GestionUserRemote");

	public static List<User> retrieveAllUsers() {
		return proxy.getAllUsers();
	}

	public static void createUser(User user) {
		proxy.persistUser(user);
	}

	public static void deleteUser(User user) {
		proxy.removeUser(user);
	}

	public static void updateUser(User user) {
		proxy.updateUser(user);
	}

	public static User retrieveOneUser(int id) {

		return proxy.searchUserByid(id);
	}
}
