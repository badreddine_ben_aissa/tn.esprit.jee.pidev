package tn.esprit.esook.client.swing;

import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.swing.JFrame;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartFrame;
import org.jfree.chart.JFreeChart;
import org.jfree.data.general.DefaultPieDataset;

import tn.edu.esprit.Pidev.Entity.Poduit;
import tn.edu.esprit.Pidev.services.GestionProduitRemote;
import tn.esprit.esook.client.swing.delegater.ProductDelegate;

import java.awt.Color;

public class Jfreestock extends JFrame {
	public Jfreestock() {
		getContentPane().setBackground(Color.WHITE);
		this.pack();
		this.setLocationRelativeTo(null);
		this.setResizable(false);

	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static void main(String[] args) {
		DefaultPieDataset data = new DefaultPieDataset();
		List<Poduit> listProduit=ProductDelegate.retrieveAllProduct();
		System.out.println(listProduit.size() + "hhhhhhh");
		for (Poduit p : listProduit) {
			data.setValue(p.getNomProd(), p.getQtStoke());
		}

		
		JFreeChart chart = ChartFactory
				.createPieChart("Quantite stock", data, true/* legend? */,
						true/* tooltips? */, false/* URLs? */);

		ChartFrame frame = new ChartFrame("Statistique", chart);
		frame.pack();
		frame.setVisible(true);
	}

}
