package tn.esprit.esook.client.swing;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.RowFilter;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import tn.edu.esprit.Pidev.Entity.Account;
import tn.edu.esprit.Pidev.Entity.Poduit;
import tn.edu.esprit.Pidev.services.GestionAdminRemote;
import tn.edu.esprit.Pidev.services.GestionProduitRemote;
import tn.esprit.esook.client.swing.delegater.ProductDelegate;

public class Produitgui extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTable tableProduityui;
	private DefaultTableModel model;
	private JTextField textField;
	private JButton btnUpdate;
	TableRowSorter<TableModel> sorter;
	Account account;
	private JTextField textFieldUpdateNom;
	private JTextField textFieldUpdateDesc;
	private JTextField textFieldUpdatePrice;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Produitgui frame = new Produitgui();
					frame.setVisible(true);
					frame.setLocationRelativeTo(null);
					frame.setPreferredSize(new Dimension(300, 300));
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public void loadTableData() {
		model = new DefaultTableModel(new Object[][] {

		}, new String[] { "id", "ProductName", "Description", "price" });
		List<Poduit> listProduit = ProductDelegate.retrieveAllProduct();

		for (Poduit p : listProduit) {

			model.addRow(new Object[] { p.getIdProduit(), p.getNomProd(),
					p.getDescription(), p.getPrice() });
		}
		tableProduityui.setModel(model);
		sorter = new TableRowSorter<TableModel>(model);
		// sorter.setRowFilter(filter);
		tableProduityui.setRowSorter(sorter);

	}

	/**
	 * Create the frame.
	 */
	public Produitgui() {
		setTitle("E-Sook");
		// this.account = a;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 815, 567);
		contentPane = new JPanel();
		contentPane.setForeground(Color.WHITE);
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 137, 724, 274);
		contentPane.add(scrollPane);

		tableProduityui = new JTable();
		scrollPane.setViewportView(tableProduityui);

		JLabel lblNewLabel = new JLabel("");
		scrollPane.setColumnHeaderView(lblNewLabel);

		final JComboBox comboBox = new JComboBox();
		comboBox.setFont(new Font("Tahoma", Font.PLAIN, 12));
		comboBox.setBounds(130, 87, 112, 39);
		comboBox.setForeground(Color.BLACK);
		comboBox.setModel(new DefaultComboBoxModel(new String[] { "idProduct",
				"nameProduct", "Price" }));
		contentPane.add(comboBox);

		textField = new JTextField();
		textField.setBounds(10, 87, 100, 39);
		contentPane.add(textField);
		textField.setColumns(10);

		JButton btnFind = new JButton("Find");
		btnFind.setIcon(new ImageIcon(
				"C:\\Users\\ramiben\\Desktop\\Icon\\1413481313_shopping-search-24.png"));
		// Image img=new
		// ImageIcon(this.getClass().getResource("/1413481313_shopping-search-24")).getImage();
		// btnFind.setIcon(new
		// ImageIcon("C:\\Users\\ramiben\\Desktop\\Icon\\1413481313_shopping-search-24.png"));
		btnFind.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnFind.setBounds(259, 87, 112, 39);
		btnFind.setForeground(Color.BLACK);
		/*
		 * btnFind.setIcon(new ImageIcon(
		 * "C:\\Users\\Mohamed-Aymen\\Downloads\\1412819689_search_magnifying_glass_find-32.png"
		 * ));
		 */
		btnFind.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				System.out.println("Start Filter");
				String text = textField.getText();

				int type = comboBox.getSelectedIndex();
				RowFilter<TableModel, Object> rf1 = null;
				// If current expression doesn't parse, don't update.
				try {
					rf1 = RowFilter.regexFilter(text, type);
				} catch (java.util.regex.PatternSyntaxException e1) {

					return;
				}
				sorter.setRowFilter(rf1);

			}
		});
		contentPane.add(btnFind);

		JButton btnUpdate = new JButton("Update");
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//
				int ligneSelectionne = tableProduityui.getSelectedRow();
				if (ligneSelectionne< -1) {
					JFrame message = new JFrame();
					JOptionPane.showMessageDialog(message, "Please select row");
				} else {
				int id = Integer.parseInt(tableProduityui.getValueAt(
						ligneSelectionne, 0).toString());
				System.out.println(id);

				try {

					Context ctx = (Context) new InitialContext();
					GestionProduitRemote gestprod = (GestionProduitRemote) ((InitialContext) ctx)
							.lookup("/Pidev/GestionProduit!tn.edu.esprit.Pidev.services.GestionProduitRemote");
					Poduit p = ProductDelegate.retrieveOneProduct(id);

					textFieldUpdateNom.setText(p.getNomProd());
					textFieldUpdateDesc.setText(p.getDescription());
					textFieldUpdatePrice.setText(p.getPrice().toString());

				} catch (Exception e2) {

					JOptionPane.showMessageDialog(null,
							"selectionnez le produit a modifier", "error",
							JOptionPane.ERROR_MESSAGE);
				}

			}
				}
		});
		btnUpdate.setBounds(588, 431, 100, 32);
		contentPane.add(btnUpdate);
		loadTableData();

		textFieldUpdateNom = new JTextField();
		textFieldUpdateNom.setBounds(51, 434, 100, 27);
		contentPane.add(textFieldUpdateNom);
		textFieldUpdateNom.setColumns(10);

		textFieldUpdateDesc = new JTextField();
		textFieldUpdateDesc.setBounds(222, 431, 86, 27);
		contentPane.add(textFieldUpdateDesc);
		textFieldUpdateDesc.setColumns(10);

		textFieldUpdatePrice = new JTextField();
		textFieldUpdatePrice.setBounds(404, 431, 86, 27);
		contentPane.add(textFieldUpdatePrice);
		textFieldUpdatePrice.setColumns(10);

		final JButton btnConfirm = new JButton("Confirm");
		btnConfirm.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				try {
					DefaultTableModel model = (DefaultTableModel) tableProduityui
							.getModel();
					int ligneSelectionne = tableProduityui.getSelectedRow();
					int id = (Integer) tableProduityui.getValueAt(
							ligneSelectionne, 0);

					Poduit p = ProductDelegate.retrieveOneProduct(id);
					p.setNomProd(textFieldUpdateNom.getText());
					p.setDescription(textFieldUpdateDesc.getText());
					ProductDelegate.updateProduct(p);

					Produitgui produitui = new Produitgui();
					produitui.setVisible(true);
					setVisible(false);

				} catch (Exception e2) {

					JOptionPane.showMessageDialog(null,
							"selectionnez le produit a modifier", "error",
							JOptionPane.ERROR_MESSAGE);
				}

			}
		});
		btnConfirm.setBounds(588, 474, 99, 32);
		contentPane.add(btnConfirm);

		JButton btnSupprimer = new JButton("Delete");
		btnSupprimer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int selectedRow = tableProduityui.getSelectedRow();
				if (selectedRow < -1) {
					JFrame message = new JFrame();
					JOptionPane.showMessageDialog(message, "Please select row");
				} else {

					int id = Integer.parseInt(tableProduityui.getValueAt(
							selectedRow, 0).toString());

					Poduit p = ProductDelegate.retrieveOneProduct(id);
					System.out.println(p.getIdProduit());
					ProductDelegate.deleteProduct(p);

					loadTableData();

				}
			}
		});
		btnSupprimer.setBounds(404, 87, 105, 37);
		contentPane.add(btnSupprimer);

		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
				UserInterface userInterface = new UserInterface();
				userInterface.setVisible(true);
			}
		});
		btnBack.setBounds(710, 0, 89, 39);
		contentPane.add(btnBack);
		
		JButton btnNewButton = new JButton("Add");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
				AddProduct1 addProduct1=new AddProduct1(null);
				addProduct1.setVisible(true);
			}
		});
		btnNewButton.setBounds(575, 87, 89, 32);
		contentPane.add(btnNewButton);
	}
}
