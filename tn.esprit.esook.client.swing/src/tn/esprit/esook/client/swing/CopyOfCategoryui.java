package tn.esprit.esook.client.swing;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.RowFilter;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import tn.edu.esprit.Pidev.Entity.Account;
import tn.edu.esprit.Pidev.Entity.Admin;
import tn.edu.esprit.Pidev.Entity.Category;
import tn.esprit.esook.client.swing.delegater.CategoryDelegate;

public class CopyOfCategoryui extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTable tableCategoryui;
	private DefaultTableModel model;
	private JTextField textField;
	TableRowSorter<TableModel> sorter;
	Account account;
	private JTextField textField_1;

	/**
	 * Launch the application.
	 */

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CopyOfCategoryui frame = new CopyOfCategoryui();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public void loadTableData() {
		// try {
		model = new DefaultTableModel(new Object[][] {

		}, new String[] { "id", "CategoryName", /* "NameAdmin" */});

		List<Category> listCategory = CategoryDelegate.retrieveAllCategories();

		for (Category c : listCategory) {

			model.addRow(new Object[] { c.getIdCategory(), c.getNameCategory(), });
		}
		tableCategoryui.setModel(model);
		sorter = new TableRowSorter<TableModel>(model);
		tableCategoryui.setRowSorter(sorter);

	}

	/**
	 * Create the frame.
	 */
	public CopyOfCategoryui(/* Account a */) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 648, 461);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 86, 612, 274);
		contentPane.add(scrollPane);

		tableCategoryui = new JTable();
		scrollPane.setViewportView(tableCategoryui);

		JButton btnAdd = new JButton("ADD");
		btnAdd.setForeground(new Color(128, 0, 0));
		btnAdd.setBounds(259, 371, 112, 31);
		btnAdd.setIcon(new ImageIcon("G:\\JEE\\image\\1412821355_Add.png"));
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (textField_1.getText().equals("")) {
					JFrame message = new JFrame();
					JOptionPane.showMessageDialog(message,
							"Please enter a category !!");

				} else {
					Category c = new Category();
					c.setNameCategory(textField_1.getText());
					Admin admin = (Admin) account;
					CategoryDelegate.createCategory(c);
					loadTableData();
				}
			}
		});
		contentPane.add(btnAdd);

		JButton btnDelete = new JButton("Delete");
		btnDelete.setBounds(364, 42, 112, 31);
		btnDelete.setForeground(new Color(139, 0, 0));
		btnDelete.setIcon(new ImageIcon(
				"G:\\JEE\\image\\1412819523_Trash_empty.png"));
		btnDelete.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				int selectedRow = tableCategoryui.getSelectedRow();

				if (selectedRow <= -1) {
					JFrame message = new JFrame();
					JOptionPane.showMessageDialog(message, "Please select row");
				} else {
					int id = Integer.parseInt(tableCategoryui.getValueAt(
							selectedRow, 0).toString());

					Category c = CategoryDelegate.retrieveOneCategory(id);
					System.out.println(c.getIdCategory());
					CategoryDelegate.deleteCategory(c);
					loadTableData();

				}
			}
		});
		contentPane.add(btnDelete);

		final JComboBox comboBox = new JComboBox();
		comboBox.setBounds(132, 42, 112, 31);
		comboBox.setForeground(new Color(165, 42, 42));
		comboBox.setModel(new DefaultComboBoxModel(new String[] { "idCategory",
				"nameCategory" }));
		contentPane.add(comboBox);

		textField = new JTextField();
		textField.setBounds(10, 42, 100, 31);
		contentPane.add(textField);
		textField.setColumns(10);

		JButton btnFind = new JButton("Find");
		btnFind.setBounds(248, 42, 112, 31);
		btnFind.setForeground(new Color(139, 0, 0));
		btnFind.setIcon(new ImageIcon(
				"G:\\JEE\\image\\1412819689_search_magnifying_glass_find-32.png"));
		btnFind.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				System.out.println("Start Filter");
				String text = textField.getText();
				int type = comboBox.getSelectedIndex();
				RowFilter<TableModel, Object> rf = null;
				try {
					rf = RowFilter.regexFilter(text, type);
				} catch (java.util.regex.PatternSyntaxException e1) {
					return;
				}
				sorter.setRowFilter(rf);

			}
		});
		contentPane.add(btnFind);

		textField_1 = new JTextField();
		textField_1.setBounds(10, 371, 207, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);

		JButton btnBack = new JButton("back");
		btnBack.setIcon(new ImageIcon("G:\\JEE\\image\\1413581314_41696.ico"));
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
				UserInterface userinterface = new UserInterface();

				userinterface.setVisible(true);
			}
		});
		btnBack.setBounds(532, 0, 100, 31);
		contentPane.add(btnBack);
		loadTableData();
	}

}
